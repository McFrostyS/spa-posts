import { POSTS } from '../../test/fixtures/posts'

export class PostsRepository {
  async getAllPosts() {
    // Intenta obtener los posts del almacenamiento local
    const posts = JSON.parse(localStorage.getItem('posts'))
    // Si no hay posts en el almacenamiento local, usa los posts de prueba
    return posts || POSTS
  }

  async createPost(post) {
    const posts = await this.getAllPosts()
    const newPost = {
      id: posts.length + 1,
      title: post.title,
      body: post.content,
      userId: 1
    }
    posts.push(newPost)
    // Guarda los posts en el almacenamiento local
    localStorage.setItem('posts', JSON.stringify(posts))
    return newPost
  }

  async deletePost(postId) {
    let posts = await this.getAllPosts()
    const index = posts.findIndex((post) => post.id === postId)
    if (index !== -1) {
      posts.splice(index, 1)
    }
    localStorage.setItem('posts', JSON.stringify(posts))
    // Devuelve los posts después de eliminar el post
    return posts
  }

  async updatePost(post) {
    let posts = await this.getAllPosts()
    const index = posts.findIndex((p) => p.id === post.id)
    if (index !== -1) {
      posts[index] = {
        id: post.id,
        title: post.title,
        body: post.content,
        userId: 1
      }
    }
    localStorage.setItem('posts', JSON.stringify(posts))
    return posts[index]
  }
}
