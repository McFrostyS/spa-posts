import { LitElement, html, css } from 'lit'
import { DeletePostUseCase } from '../usecases/delete-post.usecase'
import { UpdatePostUseCase } from '../usecases/update-post.usecase'
import { AddPostUseCase } from '../usecases/add-post.usecase'

class PostDetails extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
      mode: { type: String }
    }
  }

  static get styles() {
    return css`
      textarea {
        width: 100%;
        height: 80px;
        font-size: 16px;
      }

      label {
        display: block;
      }

      div {
        margin: 20px;
      }

      input,
      textarea {
        margin: 10px;
      }

      input {
        width: 100%;
        font-size: 16px;
      }

      a {
        text-decoration: none;
        color: white;
        margin: 0 10px;
        background-color: #fd9013;
        padding: 5px 10px;
        border-radius: 5px;
        transition: background-color 0.3s, color 0.3s;
      }

      a:hover {
        background-color: #007bff;
        color: white;
      }

      button {
        margin-left: 20px;
        padding: 5px 10px;
        background-color: #fd9013;
        color: white;
        border: none;
        border-radius: 5px;
        transition: background-color 0.3s, color 0.3s;
      }

      button:hover {
        background-color: #007bff;
        color: white;
      }
    `
  }

  constructor() {
    super()
    this.mode = 'add'
  }

  render() {
    return html`
      <button @click="${() => (this.mode = 'add')}">Add Post</button>
      ${this.mode === 'edit'
        ? html`
            <div>
              <label for="post-title">Title:</label>
              <input
                id="post-title"
                .value="${this.post?.title}"
                @input="${(e) => (this.post.title = e.target.value)}"
              />
              <br />
              <label for="post-content">Content:</label>
              <textarea
                id="post-content"
                .value="${this.post?.content}"
                @input="${(e) => (this.post.content = e.target.value)}"
              ></textarea>
              <a href="#" @click="${() => (this.mode = 'add')}">Cancel</a>
              <a href="#" @click="${this.updatePost}">Update</a>
              <a href="#" @click="${this.deletePost}">Delete</a>
            </div>
          `
        : html`
            <div>
              <label for="post-title">Title:</label>
              <input
                id="post-title"
                .value="${''}"
                @input="${(e) => (this.post.title = e.target.value)}"
              />
              <br />
              <label for="post-content">Content:</label>
              <textarea
                id="post-content"
                .value="${''}"
                @input="${(e) => (this.post.content = e.target.value)}"
              ></textarea>
              <a href="#" @click="${() => (this.mode = 'edit')}">Cancel</a>
              <a href="#" @click="${this.addPost}">Add</a>
            </div>
          `}
    `
  }

  async updatePost(event) {
    event.preventDefault()
    const title = this.shadowRoot.querySelector('#post-title').value
    const content = this.shadowRoot.querySelector('#post-content').value
    if (this.post) {
      const postModel = {
        id: this.post.id,
        title: title,
        content: content
      }
      await UpdatePostUseCase.execute([], postModel)
      alert('Post updated!')
    } else {
      console.error('this.post is undefined')
    }
  }

  async deletePost(event) {
    event.preventDefault()
    if (this.post) {
      await DeletePostUseCase.execute(this.post.id)
      alert('Post deleted!')
      window.location.href = '/posts'
    } else {
      console.error('this.post is undefined')
    }
  }

  async addPost(event) {
    event.preventDefault()
    const title = this.shadowRoot.querySelector('#post-title').value
    const content = this.shadowRoot.querySelector('#post-content').value
    const postModel = {
      title: title,
      content: content
    }
    await AddPostUseCase.execute([], postModel)
    alert('Post added!')
    window.location.href = '/posts'
  }
}

customElements.define('outlet-post-detail', PostDetails)
