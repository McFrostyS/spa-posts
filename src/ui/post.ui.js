import { LitElement, html } from 'lit'

export class PostUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object }
    }
  }

  render() {
    return html`
      <a href="#" @click="${this.showPostDetails}">
        <p id="title">${this.post?.title}</p>
      </a>
    `
  }

  showPostDetails(event) {
    event.preventDefault()
    const outlet = document.getElementById('outlet-post-detail')
    if (outlet) {
      outlet.post = this.post
    }
  }

  createRenderRoot() {
    return this
  }
}

customElements.define('post-ui', PostUI)
