import { PostsRepository } from '../repositories/posts.repository'

export class DeletePostUseCase {
  static async execute(postId) {
    const repository = new PostsRepository()
    await repository.deletePost(postId)

    return repository.getAllPosts()
  }
}
