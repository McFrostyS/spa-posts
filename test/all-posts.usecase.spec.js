import { PostsRepository } from '../src/repositories/posts.repository'
import { AllPostsUseCase } from '../src/usecases/all-posts.usecase'
import { POSTS } from './fixtures/posts'

// Mockea el repositorio para que no haga peticiones reales
jest.mock('../src/repositories/posts.repository')

describe('All posts use case', () => {
  beforeEach(() => {
    // Limpia el mock para que no haya problemas con llamadas anteriores
    PostsRepository.mockClear()
  })
  it('should get all posts', async () => {
    // Implementación del mock para que devuelva los posts de fixtures
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          // Los posts de fixtures para que no haga peticiones reales
          return POSTS
        }
      }
    })

    const posts = await AllPostsUseCase.execute()
    expect(posts.length).toBe(100)

    expect(posts[0].title).toBe(POSTS[0].title)
    expect(posts[0].content).toBe(POSTS[0].body)
  })
})
